/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.zer0.projectFighter;

import com.jme3.export.InputCapsule;
import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.OutputCapsule;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import java.io.IOException;

/**
 *
 * @author kalockhart
 */
public class AircraftControl extends AbstractControl {
    //Any local variables should be encapsulated by getters/setters so they
    //appear in the SDK properties window and can be edited.
    //Right-click a local variable to encapsulate it with getters and setters.

    // Control Inputs.
    float throttleInput = 0f;
    float liftInput = 0f;
    float dragInput = 0f;
    float pitchInput = 0f;
    float rollInput = 0f;
    float yawInput = 0f;
    
    Vector3f velocity;
    Vector3f rotationVelocity;
    Vector3f fluidFlow;
    
    float cleanMaxCL;
    float maxCl;
    
    float maxAlpha;
    float wingArea;
    
    float cD;
    float emptyWeight;
    float maxThrust;
    float loadedWeight;    
    
    Vector3f centerOfGravity;
    Vector3f centerOfLift;
    Vector3f centerOfThrust;


    
    
    @Override
    protected void controlUpdate(float tpf) {
        //TODO: add code that controls Spatial,
        //e.g. spatial.rotate(tpf,tpf,tpf);
        // calcuate forces
        
        // calculate moments
        
        // calculate worldspace
    }
    
    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //Only needed for rendering-related operations,
        //not called when spatial is culled.
    }
    
    public Control cloneForSpatial(Spatial spatial) {
        AircraftControl control = new AircraftControl();
        //TODO: copy parameters to new Control
        return control;
    }
    
    @Override
    public void read(JmeImporter im) throws IOException {
        super.read(im);
        InputCapsule in = im.getCapsule(this);
        //TODO: load properties of this Control, e.g.
        //this.value = in.readFloat("name", defaultValue);
    }
    
    @Override
    public void write(JmeExporter ex) throws IOException {
        super.write(ex);
        OutputCapsule out = ex.getCapsule(this);
        //TODO: save properties of this Control, e.g.
        //out.write(this.value, "name", defaultValue);
    }
}
