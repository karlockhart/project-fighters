/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.zer0.projectFighter;

import com.jme3.math.FastMath;

/**
 *
 * @author kalockhart
 */
public class FlightEnvironment {

    private static FlightEnvironment instance;
    //Atmospheric Constants
    public final static float Rd = 287.05f;
    public final static float Rv = 461.495f;
    public final static float RvRd = Rv/Rd;
    public final static float eso = 6.1078f;
    public final static float Lb = -0.0065f; // K/m
    public final static float M = 0.0289644f; // kg/mol
    public final static float R = 8.31432f; // N.m / mol.K
    public final static float TROPOPAUSE = 11000f; //m
    public final static float STRATOPAUSE = 20000f; //ms
    public final static float KELVIN_OFFSET = 273.15f; // K
    private final static float g0 = -9.80665f; // Acceleration due to gravity - m/s^2
    
    // Environmental Conditions.
    private float Tb = 15f; // Temperature at base - C
    private float Pb = 101325.0f; // Pressure at base - Pa
    private float rH = 0.1f;

    // Hide the no arg constructor
    private FlightEnvironment() {
    }

    public static FlightEnvironment getInstance() {
        if (instance == null) {
            instance = new FlightEnvironment();
        }
        return instance;
    }

    public float getAtmPressure(float altitude) {
        // Use the stratosphere equation if in stratosphere
        if (altitude > TROPOPAUSE && altitude < STRATOPAUSE) {
            return getStratospherePressure(altitude);
        } else {
            return getNonStratospherePressure(altitude);
        }
    }

    private float getStratospherePressure(float h) {
        float Psb = getNonStratospherePressure(TROPOPAUSE);
        float Tsb = getTemperature(TROPOPAUSE);
        return Psb * FastMath.exp((g0 * M * (h - TROPOPAUSE)) / R * Tsb);
    }

    private float getNonStratospherePressure(float h) {
        // Temperature in K.
        float TbK = Tb + KELVIN_OFFSET;
        float hb = 0f;

        if (h > TROPOPAUSE) {
            hb = STRATOPAUSE;
        }

        return Pb * FastMath.pow(
                (1 + ((Lb / TbK) * (h - hb))),
                (g0 * M) / (R * Lb));
    }

    private float getDryAirDensity(float altitude) {
        return getAtmPressure(altitude) / (Rd * (getTemperature(altitude) + KELVIN_OFFSET));
    }

    public float getHumidityRatio(float altitude) {
        float t = getTemperature(altitude) + KELVIN_OFFSET;
        float pws = FastMath.exp(77.3450f + (0.0057f * t) - (7235.0f / t)) / FastMath.pow(t, 8.2f);
        float xs = 0.62198f * pws / (getAtmPressure(altitude) - pws);
        return rH * xs;
    }

    public float getAirDensity(float altitude) {
        float x = getHumidityRatio(altitude);
        if (x == 0) {
            return getDryAirDensity(altitude);
        }
        return getDryAirDensity(altitude) * (1 + x) / (1 + RvRd * x);
    }

    public float getTemperature(float altitude) {
        //Constant in the stratosphere. Above stratopause not modeled.
        float h = altitude;
        if (altitude > TROPOPAUSE) {
            h = TROPOPAUSE;
        }

        return Tb + (Lb * (h));
    }
}
