
import com.jme3.math.Vector3f;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kalockhart
 */
public class Force {
    /*
     * The {@link Vector3f} position at which this force acts on the model.
     */
    Vector3f position;
    
    /*
     * The {@link Vector3f} components of this force.
     */
    Vector3f components;
    
    public Force(Vector3f position, Vector3f components) {
        this.position = position;
        this.components = components;
    }
    
}
